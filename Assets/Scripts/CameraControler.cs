using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour
{
    [SerializeField] private Transform player;
    private float offset = -10f;

    void Start()
    {

    }


    void Update()
    {
        if (player.position.y > -15f)
        {
            transform.position = new Vector3(player.position.x, player.position.y, offset);
        }
    }
}
