using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rolling : MonoBehaviour
{

    private float speed = 10f;

    void Update()
    {
        transform.Translate(Vector2.left * Time.deltaTime * speed);
    }
}
