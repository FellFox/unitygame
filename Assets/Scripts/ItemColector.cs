using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ItemColector : MonoBehaviour
{
    private static int pineapple = 0;

    [SerializeField] private Health health;
    [SerializeField] private Text pineappleText;
    [SerializeField] private AudioSource collectSoundEffect;

    private void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            pineapple = 0;
        }

        pineappleText.text = "" + pineapple;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Pineapple"))
        {
            Destroy(collision.gameObject);
            pineapple += 1;
            pineappleText.text = "" + pineapple;
            collectSoundEffect.Play();
        }

        if (collision.gameObject.CompareTag("Heart"))
        {
            Destroy(collision.gameObject);
            health.AddLife(1);
            collectSoundEffect.Play();

        }

    }
}
