using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private Rigidbody2D player;
    private Animator anim;
    private SpriteRenderer sprite;
    private BoxCollider2D coll;
    [SerializeField] private LayerMask jumpableGround;

    private float horizontalMove = 0f;
    private float moveSpeed = 10f;
    [SerializeField] private float jumpForce = 15f;
    private enum MovementState { idle, running, jumping, falling }

    [SerializeField] private AudioSource jumpSoundEffect;


    void Start()
    {
        player = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        coll = GetComponent<BoxCollider2D>();
    }


    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal");
        if (player.bodyType != RigidbodyType2D.Static)
        {
            player.velocity = new Vector2(horizontalMove * moveSpeed, player.velocity.y);
        }

        UpdateAnimationState();


    }

    private void UpdateAnimationState()
    {

        MovementState state;

        if (Input.GetButtonDown("Jump") && isGrounded())

        {
            player.velocity = new Vector2(player.velocity.x, jumpForce);
            jumpSoundEffect.Play();
        }

        if (horizontalMove > 0f)
        {
            state = MovementState.running;
            sprite.flipX = false;
        }
        else if (horizontalMove < 0f)
        {
            state = MovementState.running;
            sprite.flipX = true;
        }
        else
        {
            state = MovementState.idle;
        }

        if (player.velocity.y > .1f)
        {
            state = MovementState.jumping;
        }
        else if (player.velocity.y < -.1f)
        {
            state = MovementState.falling;
        }

        anim.SetInteger("state", (int)state);
    }

    private bool isGrounded()
    {
        return Physics2D.BoxCast(coll.bounds.center, coll.bounds.size, 0f, Vector2.down, .1f, jumpableGround);
    }
}
