using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Health : MonoBehaviour
{
    private Animator anim;
    private Rigidbody2D rb;
    [SerializeField] private AudioSource deathSoundEffect;
    private Vector2 startingPossition;
    public static float currentHealth;
    public float currentHealthBar;


    void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            currentHealth = 3f;
        }
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        currentHealthBar = currentHealth;
        startingPossition = transform.position;
    }

    void Update()
    {
        currentHealthBar = currentHealth;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Trap"))
        {
            TakeDamage(1);
        }

    }

    public void AddLife(float amount)
    {
        if (currentHealth < 3)
        {
            currentHealth += amount;
        }
    }


    public void TakeDamage(float amount)
    {
        currentHealth -= amount;
        rb.bodyType = RigidbodyType2D.Static;
        deathSoundEffect.Play();
        anim.SetTrigger("death");
        currentHealthBar = currentHealth;
        // Invoke("Respawn", 0.5f);
        if (currentHealth == 0)
        {
            Invoke("GameOver", 0.5f);
        }

    }

    private void Respawn()
    {
        rb.bodyType = RigidbodyType2D.Dynamic;
        transform.position = startingPossition;
        anim.SetInteger("state", 0);

    }

    private void GameOver()
    {

        SceneManager.LoadScene("Game Over Scene");

    }


}
