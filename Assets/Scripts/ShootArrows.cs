using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootArrows : MonoBehaviour
{
    [SerializeField] private ArrowMovement ArrowPrefab;
    private float startDelay = 2f;
    private float interval = 2f;

    void Start()
    {
        InvokeRepeating("ShootArrow", startDelay, interval);
    }


    private void ShootArrow()
    {
        Instantiate(ArrowPrefab, transform.position, ArrowPrefab.transform.rotation);
    }
}
