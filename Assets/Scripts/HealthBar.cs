using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Health playerHealth;
    [SerializeField] private Image totalplayerHealth;
    [SerializeField] private Image currentPlayerHealth;



    void Start()
    {
        totalplayerHealth.fillAmount = playerHealth.currentHealthBar / 10;
    }


    void Update()
    {
        currentPlayerHealth.fillAmount = playerHealth.currentHealthBar / 10;
    }
}
