using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowMovement : MonoBehaviour
{
    private float speed = 10f;
    public float destroyDistance = 1f;

    private void OnCollisionEnter2D(Collision2D other)

    {

        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }

    }
    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);
        if (transform.position.x < destroyDistance)
        {
            Destroy(gameObject);
        }
    }
}
